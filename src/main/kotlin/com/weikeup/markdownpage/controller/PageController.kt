package com.weikeup.markdownpage.controller

import com.weikeup.markdownpage.model.Page
import com.weikeup.markdownpage.model.PageRepository
import com.weikeup.markdownpage.model.SavePageRequestBody
import net.bytebuddy.utility.RandomString
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.util.*
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletResponse

@Controller
@RequestMapping("/")
class PageController {
    private val log = LoggerFactory.getLogger(javaClass)

    @Autowired
    private lateinit var pageRepository: PageRepository

    @GetMapping("/")
    fun newPage(@CookieValue("aid") authorId: String?, resp: HttpServletResponse): String {
        if (Objects.isNull(authorId)) {
            val aid = RandomString.make(10)
            log.debug("Generating new author id: $aid")
            val authorIdCookie = Cookie("aid", aid)
            authorIdCookie.maxAge = 60 * 60 * 24 * 365 * 10
            resp.addCookie(authorIdCookie)
        }
        return "index"
    }

    @PostMapping("/")
    @ResponseBody
    fun savePage(@RequestBody req: SavePageRequestBody, @CookieValue("aid") aid: String): String {
        val title = Regex("^# (.*)$", RegexOption.MULTILINE)
                .find(req.rawMD)?.groups?.get(1)?.value ?: "Untitled Page"
        val page = Page(id = RandomString.make(6), content = req.rawMD, author = aid, title = title)
        pageRepository.save(page)
        return "p${page.id}"
    }

    @GetMapping("/p{pageId}")
    fun showPage(@PathVariable pageId: String, @CookieValue("aid") aid: String?, model: Model): String {
        val page = pageRepository.findById(pageId).orElseThrow()
        ++page.numOfViews
        pageRepository.save(page)
        val author = if (page.author.startsWith("u") && page.author.length > 6) page.author else "Anonymous"
        val canEdit = Objects.isNull(aid).not() && (page.author == aid) //TODO: Need to Change Authorize Method
        model.addAttribute("canEdit", canEdit)
        model.addAttribute("author", author)
        model.addAttribute("page", page)
        return "page"
    }

    @GetMapping("/e{pageId}")
    fun editPage(@PathVariable pageId: String, @CookieValue("aid") aid: String?, model: Model): String {
        val page = pageRepository.findById(pageId)
        when {
            page.isEmpty -> {
                throw ResponseStatusException(HttpStatus.NOT_FOUND)
            }
            page.get().author != aid -> {
                throw ResponseStatusException(HttpStatus.FORBIDDEN)
            }
        }
        model.addAttribute("page", page.get())
        return "edit_page"
    }

    @PostMapping("/e{pageId}")
    @ResponseBody
    fun saveEditedPage(@RequestBody req: SavePageRequestBody, @PathVariable pageId: String, @CookieValue("aid") aid: String?) {
        val page = pageRepository.findById(pageId)
        when {
            page.isEmpty -> {
                throw ResponseStatusException(HttpStatus.NOT_FOUND)
            }
            page.get().author != aid -> {
                throw ResponseStatusException(HttpStatus.FORBIDDEN)
            }
            else -> {
                val p = page.get()
                val title = Regex("^# (.*)$", RegexOption.MULTILINE)
                        .find(req.rawMD)?.groups?.get(1)?.value ?: "Untitled Page"
                p.title = title
                p.content = req.rawMD
                pageRepository.save(p)
            }
        }
    }

    @DeleteMapping("/e{pageId}")
    @ResponseBody
    fun deletePage(@PathVariable pageId: String, @CookieValue("aid") aid: String?) {
        val page = pageRepository.findById(pageId)
        when {
            page.isEmpty -> {
                throw ResponseStatusException(HttpStatus.NOT_FOUND)
            }
            page.get().author != aid -> {
                throw ResponseStatusException(HttpStatus.FORBIDDEN)
            }
            else -> {
                pageRepository.deleteById(page.get().id)
            }
        }
    }

    @GetMapping("/exp")
    fun listPages(model: Model): String {
        val pages = pageRepository.findAll()
        model.addAttribute("pages", pages)
        return "explore"
    }
}