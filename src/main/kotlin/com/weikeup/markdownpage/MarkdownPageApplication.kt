package com.weikeup.markdownpage

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MarkdownPageApplication

fun main(args: Array<String>) {
    runApplication<MarkdownPageApplication>(*args)
}
