package com.weikeup.markdownpage.model

import java.sql.Timestamp
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "pages")
data class Page(
        @Id
        var id: String = "",
        @Column
        var title: String = "",
        @Column
        @Lob
        var content: String = "",
        @Column
        var author: String = "",
        @Column
        var numOfViews: Int = 0,
        @Column
        var createTimestamp: Timestamp = Timestamp(Date().time)
)