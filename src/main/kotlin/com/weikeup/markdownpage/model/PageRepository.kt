package com.weikeup.markdownpage.model

import org.springframework.data.jpa.repository.JpaRepository

interface PageRepository : JpaRepository<Page, String>