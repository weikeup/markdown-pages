package com.weikeup.markdownpage.model

data class SavePageRequestBody(
        val rawMD: String
)
